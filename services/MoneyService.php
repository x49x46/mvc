<?php

namespace services;

use models\Money;
use classes\DBConnection;

class MoneyService
{
    public function writeOff($user_id, $sum)
    {
        $config = include 'configs/db.php';

        $connection = new DBConnection($config);

        $connection->begin();

        /**@var $model \models\Money*/
        if (!$model = Money::find($connection, ['user_id' => $user_id])) {
            return false;
        }

        $new_balance = $model->writeOff($sum);

        if ($model->save()) {

            $connection->commit();

            return $new_balance;
        }

        $connection->rollback();

        return false;
    }

    public function getBalance($user_id)
    {
        $config = include 'configs/db.php';

        $connection = new DBConnection($config);

        /**@var $model \models\Money*/
        if (!$model = Money::find($connection, ['user_id' => $user_id])) {
            return false;
        }

        return $model->getBalance();
    }
}