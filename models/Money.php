<?php

namespace models;

use classes\BaseModel;

class Money extends BaseModel
{
    public $id;
    public $user_id;
    public $balance;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    public function writeOff($sum)
    {
        $sum = intval($sum);

        if ($sum <= 0) {
            return false;
        }

        $balance = $this->getBalance();

        if ($sum > $balance) {
            return false;
        }

        $new_balance = $balance - $sum;

        $this->setBalance($new_balance);

        return $new_balance;
    }
}