<?php

$config = include 'configs' . DIRECTORY_SEPARATOR . 'db.php';

//Class autoload
spl_autoload_register(function($class) {
    $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);

    $file = $class . '.php';

    if (file_exists($file)) {
        include_once $file;
    }
});

//Controller autoload
spl_autoload_register(function($class) {
    $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);

    $file = 'controllers' . DIRECTORY_SEPARATOR . $class . '.php';

    if (file_exists($file)) {
        include_once $file;
    }
});

use classes\Application;

(new Application())->run();
