<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<div class="row">
    <div class="col-2"></div>
    <div class="col-8">
        <div class="card mt-3">
            <div class="card-header">
                <h4>
                    Списание
                </h4>
            </div>
            <div class="card-body">
                Текущая сумма: <?=$sum?>
                <hr>
                <form method="post">
                    <div class="form-group">
                        <input type="text" name="sum" class="form-control">
                    </div>

                    <button class="btn btn-info">Списать</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-2"></div>
</div>