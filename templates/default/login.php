<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<div class="row">
    <div class="col-4"></div>
    <div class="col-4">
        <div class="card mt-3">
            <div class="card-header">
                <h5>Вход</h5>
            </div>
            <div class="card-body">
                <form method="post">
                    <div class="form-group">
                        <input name="login" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <input name="pass" type="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success">Войти</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-4"></div>
</div>

