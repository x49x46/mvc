<?php

namespace utils;

class StringUtils
{
    static public function CamelCaseToUnderscore($str)
    {
        $result = '';
        $parts = str_split($str);

        foreach ($parts as $key => $part) {
            if (ctype_upper($part) && $key != 0) {
                $result .= '_';
            }

            $result .= mb_strtolower($part);
        }

        return $result;
    }
}