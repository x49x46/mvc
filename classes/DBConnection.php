<?php


namespace classes;


class DBConnection
{
    /**@var $pdo \PDO */
    private $pdo = null;

    public function __construct($config)
    {
        if (empty($config) || !is_array($config)) {
            throw new \Exception('Empty config of database connection!');
        }

        if (!isset($config['host']) || !isset($config['name']) || !isset($config['user']) || !isset($config['pass'])) {
            throw new \Exception('Error in config of database connection!');
        }

        $dsn = 'mysql:dbname=' . $config['name'] . ';host=' . $config['host'];

        $this->pdo = new \PDO($dsn, $config['user'],$config['pass']);
    }

    public function exec($sql, $params = [])
    {
        $statement = $this->pdo->prepare($sql);
        $result = $statement->execute($params);

        if ($statement->columnCount() === 0) {
            return $result;
        }

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function begin()
    {
        $this->pdo->beginTransaction();
    }

    public function commit()
    {
        $this->pdo->commit();
    }

    public function rollback()
    {
        $this->pdo->rollBack();
    }

    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }
}