<?php


namespace classes;


abstract class BaseController
{
    abstract public function defaultAction();
}