<?php


namespace classes;


use http\Exception;
use utils\StringUtils;
use classes\DBConnection;

abstract class BaseModel
{
    /**@var $connection \classes\DBConnection*/
    private $connection = null;

    public $exclude = [
        'exclude',
        'connection'
    ];

    public $id;

    public function __construct($connection)
    {
        if (empty($connection)) {
            throw new \Exception('No database connection!');
        }


        $this->connection = $connection;
    }

    protected function delete()
    {
        if ($this->isNew()) {
            return false;
        }

        $sql = 'DELETE FROM ' . $this->table() . ' WHERE id = ' . $this->id;

        return $this->connection->exec($sql);
    }

    protected function update()
    {
        $table = $this->table();
        $properties = $this->getProperties();
        $fields = [];

        foreach ($properties as $property) {
            $fields[] = "`$property` = :$property";
        }

        $fields = implode(',', $fields);

        return "UPDATE `$table` SET $fields WHERE `id` = :id";
    }

    public function insert()
    {
        $table = $this->table();
        $properties = $this->getProperties();
        $properties_titles = [];
        $properties_places= [];

        foreach ($properties as $property) {
            $properties_titles[] = "`$property`";
            $properties_places[] = ":$property";
        }

        $titles = implode(',', $properties_titles);
        $places = implode(',', $properties_places);

        return "INSERT INTO `$table` ($titles) VALUES ($places)";
    }

    public function isNew()
    {
        return empty($this->id);
    }

    public function getPropertiesAndValues()
    {
        $properties = [];

        foreach ($this as $property => $value) {
            if (in_array($property, $this->exclude)) {
                continue;
            }

            if (!empty($value) || is_numeric($value)) {
                $properties[$property] = $value;
            }
        }

        return $properties;
    }

    public function getProperties()
    {
        return array_keys($this->getPropertiesAndValues());
    }

    public function getValues()
    {
        return array_values($this->getPropertiesAndValues());
    }

    public function save()
    {
        if ($this->isNew()) {
            $sql = $this->insert();
        } else {
            $sql = $this->update();
        }

        $res = $this->connection->exec($sql, $this->getPropertiesAndValues());

        if ($this->isNew()) {
            return $this->id = $this->connection->lastInsertId();
        }

        return $res;
    }

    public function table()
    {
        return StringUtils::CamelCaseToUnderscore((new \ReflectionClass($this))->getShortName());
    }

    static public function find($connection, $params, $condition = '', $order = null)
    {
        if (is_numeric($params)) {
            $params = intval($params);
            $params = ['id' => $params];
        }

        if (!is_array($params)) {
            return false;
        }

        $objects = self::findAll($connection, $params, $condition, $order, 1);

        return empty($objects) ? false : $objects[0];
    }

    static public function findAll($connection, array $params = [], $condition = '', $order = null, $limit = null)
    {
        $order = (!$order) ? ' ORDER BY `id` ' : $order;
        $limit = $limit ? ' LIMIT ' . $limit : $limit;

        $class = get_called_class();

        /**@var $object BaseModel */
        $object = new $class($connection);

        $where = '';

        if ($condition) {
            $where .= $condition;
        }

        $fields = [];

        foreach ($params as $property => $value) {
            $fields[] = "`$property` = :$property";
        }

        $where = implode(' AND ', $fields);
        $sql = 'SELECT * FROM `' . $object->table() . '` WHERE ' . $where . ' ' . $order . ' ' . $limit;
        $res = $connection->exec($sql, $params);
        $result = false;

        if ($res) {
            foreach ($res as $row) {
                $object = new $class($connection);

                foreach ($row as $field => $value) {
                    if (property_exists($object, $field)) {
                        $object->$field = $value;
                    }
                }

                $result[] = $object;
            }
        }

        return $result;
    }
}