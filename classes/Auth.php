<?php


namespace classes;

use models\User;

class Auth
{
    public function __construct()
    {
        session_start();
        session_regenerate_id();
    }

    public function login($login, $pass)
    {
        $config = include 'configs/db.php';
        $connection = new DBConnection($config);
        $pass = sha1($pass);
        $model = User::find($connection, ['login' => $login, 'pass' => $pass]);

        if ($model) {
            $_SESSION['user'] = $login;
            $_SESSION['user_id'] = $model->id;
            return true;
        }

        return false;
    }

    public function logout()
    {

    }

    public function isLogged()
    {
        return isset($_SESSION['user']);
    }
}