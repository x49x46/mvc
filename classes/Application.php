<?php

namespace classes;

class Application
{
    const CONTROLLER_SUFFIX = 'Controller';
    const ACTION_SUFFIX = 'Action';
    const DEFAULT_CONTROLLER = 'Default';
    const DEFAULT_ACTION = 'default';

    public function __construct()
    {
    }

    public function run()
    {
        $controller_name = self::DEFAULT_CONTROLLER;
        $action_name = self::DEFAULT_ACTION;
        $route = $this->getRoute();

        if ($route) {
            if (!empty($route[0])) {
                $controller_name = ucfirst($route[0]);
            }
            if (!empty($route[1])) {
                $action_name = ucfirst($route[1]);
            }
        }

        $controller_name .= self::CONTROLLER_SUFFIX;
        $action_name .= self::ACTION_SUFFIX;

        $controller = new $controller_name();

        echo $controller->{$action_name}();

        session_write_close();
    }

    public function getRoute()
    {
        return isset($_GET['r']) ? explode('/', $_GET['r']) : false;
    }
}