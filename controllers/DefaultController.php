<?php

use classes\BaseController;
use classes\Auth;

class DefaultController extends BaseController
{
    public function defaultAction()
    {
        if (!empty($_POST['login']) && !empty($_POST['pass'])) {
            if ((new Auth())->login($_POST['login'], $_POST['pass'])) {
                header('Location: ?r=cabinet/account');

            }
        }

        include 'templates/default/login.php';
    }
}