<?php

use classes\BaseController;
use services\MoneyService;
use classes\Auth;
use models\Money;
use classes\DBConnection;

class CabinetController extends BaseController
{
    public function defaultAction()
    {
        // TODO: Implement defaultAction() method.
    }

    public function accountAction()
    {
        if (!(new Auth())->isLogged()) {
            header('Location: /');
        }

        if (!empty($_POST['sum'])) {
            $sum = (new MoneyService())->writeOff($_SESSION['user_id'], $_POST['sum']);
        }

        if (!isset($sum) || $sum === false) {
            $sum = (new MoneyService())->getBalance($_SESSION['user_id']);
        }

        include 'templates/cabinet/account.php';
    }
}